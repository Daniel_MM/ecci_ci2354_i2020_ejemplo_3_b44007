package cr.ac.ucr.ecci.eseg.fragmentos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class TextFragment extends Fragment {

    private TextView textview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_text, container, false);
        textview = view.findViewById(R.id.textView1);
        return view;
    }

    void changeTextProperties(int fontsize, String text){
        textview.setTextSize(fontsize);
        textview.setText(text);
    }
}